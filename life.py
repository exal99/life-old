#!/usr/bin/env python3.3 

import matrix
from ui import *
from lifetest import life

@life(1)
def create_new_world(colums, rows):
    return matrix.Matrix(colums, rows)
    
@life(2)
def stay_alive(populated, nabours):
    if populated:
        if nabours >= 2 and nabours <= 3:
            return True
        else:
            return False
    else:
        return False

@life(3)
def birth(populated, nabours):
    if not populated:
        if nabours == 3:
            return True
        else:
            return False
    else:
        return False

@life(4)
def is_populated(world, x, y):
    if x > (world.get_num_columns() - 1) or y > (world.get_num_rows() - 1):
        return False
    elif x < 0 or y < 0:
        return False
    else:
        return world.get(x, y)

@life(5)
def num_nabours(world, x, y):
    new_x = x - 1
    new_y = y - 1
    nabours = 0
    testy = True
    while testy:
        if new_x == x and new_y == y:
            new_x += 1
        else:
            if is_populated(world, new_x, new_y):
                nabours += 1
            if new_x == x + 1 and new_y == y + 1:
                testy = False
            elif new_x == x + 1:
                new_x = x - 1
                new_y += 1
            else:
                new_x += 1
    return nabours

@life(6)
def new_population(world, x, y, nabours):
    if is_populated(world, x, y):
        if stay_alive(True, nabours):
            return True
        else:
            return False
    else:
        if birth(False, nabours):
            return True
        else:
            return False

#@life(7)
#def new_world(world):
#    colum = world.get_num_columns()
#   row = world.get_num_rows()
#    New_world = create_new_world(colum, row)
#    for y in New_world:
#        for x in New_world:
#            nabours = num_nabours(New_world, x, y)
#            if new_population(New_world, x, y, nabours):
#                New_world.update(x, y, True)
#    return colum, row
@life(7)
def new_world(world):
    colums = world.get_num_columns()
    rows = world.get_num_rows()
    New_world = create_new_world(colums, rows)
    testing = True
    colum = 0
    row = 0
    while testing:
        if colum > colums and row > rows:
            testing = False
        elif colum > colums:
            colum = 0
            row += 1
        else:
            nabours = num_nabours(world, colum, row)
            if new_population(world, colum, row, nabours):
                New_world.update(colum, row, True)
                colum += 1
            else:
                colum += 1
    return New_world

@life(8)
def num_population(world):
    colums = world.get_num_columns()
    rows = world.get_num_rows()
    pop_count = 0
    testing = True
    colum = 0
    row = 0
    while testing:
        if colum > colums and row > rows:
            testing = False
        elif colum > colums:
            colum = 0
            row += 1
        else:
            if is_populated(world, colum, row):
                pop_count += 1
            colum += 1
    return pop_count
    
@life(9)
def nothing_happens(world, next_world):
    if world == next_world:
        return True
    else:
        return False

def mainrun(patern):
    if not patern:
        world = load_world()
    else:
        world = load_world(patern)
    run = True
    while run:
        old_world = world
        population = num_population(world)
        show_world(world, population)
        world = new_world(world)
        if nothing_happens(old_world, world):
            run = False
            print("The new world was the same as the last and therfore it was killed")

def input_pattern():
    done = False
    patern = []
    while not done:
        patt = input("What do you whant your pattern to be? ")
        if not patt:
            done = True
        else:
            patern.append(patt)
    return patern
        
if __name__ == '__main__':
    patt = input_pattern()
    mainrun(patt)
