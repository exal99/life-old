import pprint

class World(object):
    """Defines a world for life cells."""
    def __init__(self, cols, rows, default=False):
        self.num_rows = rows
        self.num_cols = cols

        self.start_cols = [999999 for r in range(rows)]
        self.end_cols = [0 for r in range(rows)]
        self.cells = [[default for c in range(cols)] for r in range(rows)]
        self.cell_counts = [0 for r in range(rows)]
        self.living_rows = set()

    def get_num_rows(self):
        return self.num_rows

    def get_num_columns(self):
        return self.num_cols

    def __str__(self):
        return pprint.pformat(self.cells)

    def get_row(self, row):
        return self.cells[row]

    def is_alive(self, col, row):
        return self.cells[row][col]

    def get_boundaries(self):
        if self.living_rows:
            return ((min(self.living_rows), max(self.living_rows)),(min(self.start_cols), max(self.end_cols)))
        else:
            return None

    def alive(self, col, row):
        if self.cells[row][col]: return
        self.cells[row][col] = True

        self.start_cols[row] = min(self.start_cols[row], col)
        self.end_cols[row] = max(self.end_cols[row], col)

        self.cell_counts[row] = self.cell_counts[row]  + 1
        self.living_rows.add(row)

    def dead(self, col, row):
        if not self.cells[row][col]: return

        self.cells[row][col] = False
        self.cell_counts[row] = max(self.cell_counts[row]-1, 0)

        if self.cell_counts[row] > 0:
            if self.start_cols[row] == col:
                self.start_cols[row] = self.get_row(row).index(True, col + 1)
            if self.end_cols[row] == col:
                cur_row = self.get_row(row)
                self.end_cols[row] = self.get_num_columns() - cur_row[::-1].index(True) - 1
        else:
            self.start_cols[row] = 999999
            self.end_cols[row] = 0
            self.living_rows.discard(row)

    def __eq__(self, other):
        if type(other) is type(self):
            if isinstance(other, self.__class__):
                return self.__dict__ == other.__dict__
        return False

    def __ne__(self, other):
        return not self.__eq__(other)