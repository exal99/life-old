 Life
======

Spelet "Life" eller "Conway's Game of Life" skapades matematiken John Horton Conway och
presenterades i tidningen "Scientific America" i oktober 1970.
Spelet, eller tankeexperimentet, är en simulator över hur en
koloni av enkla organismer kan utveckas.
Det är ett "zero-player"-spel. Man kan efter grunduppställningen inte 
ge någon input, utan spelet fortsätter av sig själv.

FILM

Spelvärlden är två dimmensionell och bestående av ett rutnät, precis 
som ett rutat kollegieblock. 

Varje ruta har åtta grannar:

Bild:

123
4*5
678

Varje ruta kan innehålla en individ eller vara tom.
För enkelhetens skulle så rekommenderas att världen är platt och ändlig,
dvs det finns en kant och rutor utanför kanten är tomma. Så en ruta
längs kanterna ser ut så här:

 23   *1
 *5   23
 78

Med andra ord, en ruta längs kanten kan ha som mest fem stycken individer som grannar, och en ruta i ett hörn kan upp till tre grannar.

# De tre reglerna:

Det är tre regler som beskriver hur populationen i världen utvecklas i tiden.

-  Överlevnad: Varje individ med två eller tre individer i grannrutorna överlever till nästa omgång
-  Födsel: Det uppstår en ny individ i varje tom ruta som har tre individer i grannrutorna
-  Döden: Om de två reglerna ovan inte inträffar ska rutan i fråga vara tom

Alla rutor utvärderar simultant sin situation. Man behöver hålla sig med två världar
parallellt, en som är "nuet" och en som är "framtiden", där man fyller i hur det ska bli.

# Starta
   * Börja med att klona life-gitten ifrån bitbucket `https://bitbucket.org/patwic/life`
   * Provkör funktionerna load_world() och show_world() i ui, med from ui import (Se example.py) för att få en
     känsla av hur det kommer att se ut. example.py visar upp ett initial mönster.

# Nerbrytning av spelet i delar
  * Vi har en nuvarande värld med invånare och vi behöver en tom värld där vi ska skapa hur nästa omgång ska se ut.
  * För att kunna placera ut invånare i den nya världen behöver vi implementera de tre reglerna. 
  * Vi måste kunna applicera de tre reglerna på varje ruta i den nuvarande världen och ta svaret o lagra det i nästa världen.
  * Vad behövs för de tre reglerna?
     - Om det finns en invånare eller inte i rutan i den nuvarande världen.
     - Antalet invånare i grannrutorna.
  * Vad behövs för att få fram antalet grannar i grannrutorna?
     - Koordinater till de åtta grannrutorna.
     - Hantering av kanterna. Dvs om någon av grannrutorna ligger utanför världen så måste den räknas som tom.
     - Avläsning av invånare i grannrutorna och summering av dom.

# Övningar

  * Definera en funktion som skapar en ny värld där nästa omgång ska fyllas i. 
    Annotera funktionen med:
    `@life(1)`
   
  * Importera `matrix` 
  * I din nya funktion instansiera en ny instans av Matrix som du sedan returnerar. Du behöver 
    läsa ut dimensionerna ifrån den gamla världen, `world` och ge dom som argument till din nya funktion och vidare som
    argument till Matrix när du skapar den nya world objektet. Först antal kolumner sedan antal rader.
    Den nya världen behöver vara lika stor som den gamla.
    Testa din funktion genom att efter importeringen av matrix lägga till raden: `from lifetest import life` för att
    ladda in testfallen.
    Efter och utanför din funktion kan du skriva `life.test(1)` för att testa din funktion. `life.test(1)` returnerar
    inte, utan avslutar programmet efter testet. Du kan även skriva `life.test()` för att testa alla funktioner som du har
    annoterat.

    `#!/usr/bin/env python3.3

    from ui import *
    from lifetest import life
    import matrix

    @life(1)
    def create_new_world(columns, rows):
    	return matrix.Matrix(...)

    w = load_world()
    columns = w.get..
    rows = w.get...
    new_world = create_new_world(columns, rows)

    life.test(1)`


  * Implementera reglerna "Överlevnad" och "Födseln" i två olika funktioner som ska ta argumenten: om nuvarande rutan har en invånare och antal grannar.

    Annotera `Stay alive:` regeln med:
    `@life(2)`

    Annotera `Birth:` regel med:
    `@life(3)`

    `Death:` regeln implementeras lättast som en `else`-sats till de andra två reglerna och det görs i ett senare steg.
    

    Funktionerna ska returnera `True` om regeln är uppfyld, `False` annars. 
    Kontrollera om dessa två funktioner blev rätt genom flytta ner `life.test()` efter den sista funktionen.
    Du behöver inte implementera bägge funktioner innan du testar om en funktion blev rätt, tex för att testa
    "Birth" regeln kan göras genom att anropa `life.test(3)`

    Om du vill kan du använda pybat få lite hjälp med Stay Alive och Birth reglerna.
    [länkar: http://bat.patwic.com/balls/show/life-rules-birth http://bat.patwic.com/balls/show/life-rules-alive]

  * Implementera en funktion som returnerar `True` om en ruta är bebodd eller `False` om den är tom.
    Som inargument behövs `world` och två koordinater för att peka ut vilken ruta.

    Här behövs det kontrolleras om rutan finns, då tanken är att använda denna funktion för att ta reda på
    antal grannar en ruta har och denna funktion måste också fungera längs kanterna. Om koordinaterna
    pekar på en ruta utanför världen så ska `False` returneras. Med utanför menas koordinater som är 
    mindre än noll eller större eller lika med antalet kolumner resp rader i världen.

    Annotera funktionen med:
    `@life(4)`
    och kontrollera om den blev rätt med `life.test(4)`

  * Här kan du testköra lite kod som försöker visa problematiken med kanterna.

    Det första exemplet går ut på att visa förhållandet koordinater jämnt emot rader och kolumner. 
    Dvs tex om world.get_num_rows() returnerar 8 så är den sista raden som finns rad nummer 7.

    Exemplet kommer att fallera då det försöker accessa en ruta som inte finns.

<iframe src="http://pythontutor.com/iframe-embed.html#code=%0A%23+A+very+small+%22world%22%0A%23+filled+with+letters%0A%23+to+visualize.%0Aw+%3D+%5B%5B'a',+'b',+'c'%5D,+%0A+++++%5B'd',+'e',+'f'%5D,+%0A+++++%5B'g',+'h',+'i'%5D,+%0A+++++%5B'j',+'k',+'l'%5D%5D%0A%0Arows+%3D+len(w)%0Acolumns+%3D+len(w%5B0%5D)%0A%23+Note%3A+it+is+w%5Brow%5D%5Bcolumn%5D%0Aw00+%3D+w%5B0%5D%5B0%5D%0Aw11+%3D+w%5B1%5D%5B1%5D%0A%0A%23+The+next+line+won't+work!%0Aw43+%3D+w%5B4%5D%5B3%5D%0A&amp;cumulative=false&amp;heapPrimitives=false&amp;drawParentPointers=false&amp;textReferences=false&amp;showOnlyOutputs=false&amp;py=3&amp;curInstr=4&amp;codeDivWidth=350&amp;codeDivHeight=500" width="900" height="650"></iframe><
    
    http://pythontutor.com/visualize.html#code=%0A%23+A+very+small+%22world%22%0A%23+filled+with+letters%0A%23+to+visualize.%0Aw+%3D+%5B%5B'a',+'b',+'c'%5D,+%0A+++++%5B'd',+'e',+'f'%5D,+%0A+++++%5B'g',+'h',+'i'%5D,+%0A+++++%5B'j',+'k',+'l'%5D%5D%0A%0Arows+%3D+len(w)%0Acolumns+%3D+len(w%5B0%5D)%0A%23+Note%3A+it+is+w%5Brow%5D%5Bcolumn%5D%0Aw00+%3D+w%5B0%5D%5B0%5D%0Aw11+%3D+w%5B1%5D%5B1%5D%0A%0A%23+The+next+line+won't+work!%0Aw43+%3D+w%5B4%5D%5B3%5D%0A&mode=display&cumulative=false&heapPrimitives=false&drawParentPointers=false&textReferences=false&showOnlyOutputs=false&py=3&curInstr=4

    Det andra exemplet visa vad som händer om man ger negativa koordinater.

iframe width="900" height="700" frameborder="0" src="http://pythontutor.com/iframe-embed.html#code=%0A%23+A+very+small+%22world%22+%0A%23+filled+with+letters%0A%23+to+visualize.%0Aw+%3D+%5B%5B'a',+'b',+'c'%5D,+%0A+++++%5B'd',+'e',+'f'%5D,%0A+++++%5B'g',+'h',+'i'%5D,%0A+++++%5B'j',+'k',+'l'%5D%5D%0A%0Arows+%3D+len(w)%0Acolumns+%3D+len(w%5B0%5D)%0A%23+Note%3A+it+is+w%5Brow%5D%5Bcolumn%5D%0Aw00+%3D+w%5B0%5D%5B0%5D%0Aw02+%3D+w%5B0%5D%5B2%5D%0Aw32+%3D+w%5B3%5D%5B2%5D%0A%0A%23+These+kinda+works,+%0A%23+since+-1+wraps.%0Awm1m1%3D+w%5B-1%5D%5B-1%5D%0Aw0m1%3D+w%5B0%5D%5B-1%5D&amp;cumulative=false&amp;heapPrimitives=false&amp;drawParentPointers=false&amp;textReferences=false&amp;showOnlyOutputs=false&amp;py=3&amp;curInstr=4&amp;codeDivWidth=350&amp;codeDivHeight=500"> </iframe>

       http://pythontutor.com/visualize.html#code=%0A%23+A+very+small+%22world%22+%0A%23+filled+with+letters%0A%23+to+visualize.%0Aw+%3D+%5B%5B'a',+'b',+'c'%5D,+%0A+++++%5B'd',+'e',+'f'%5D,%0A+++++%5B'g',+'h',+'i'%5D,%0A+++++%5B'j',+'k',+'l'%5D%5D%0A%0Arows+%3D+len(w)%0Acolumns+%3D+len(w%5B0%5D)%0A%23+Note%3A+it+is+w%5Brow%5D%5Bcolumn%5D%0Aw00+%3D+w%5B0%5D%5B0%5D%0Aw02+%3D+w%5B0%5D%5B2%5D%0Aw32+%3D+w%5B3%5D%5B2%5D%0A%0A%23+These+kinda+works,+%0A%23+since+-1+wraps.%0Awm1m1%3D+w%5B-1%5D%5B-1%5D%0Aw0m1%3D+w%5B0%5D%5B-1%5D&mode=display&cumulative=false&heapPrimitives=false&drawParentPointers=false&textReferences=false&showOnlyOutputs=false&py=3&curInstr=4

    Det ser ut som om det fungerar, men notera värdet i wm1m1, det är samma som w32. Och w02 är samma som w0m1. 
    Med andra ord, för att världen inte ska "wrappa" i negativa riktningar måste en koll för negativa koordinater läggas till o hanteras.
 
  * Nu behövs en funktion som returnerar antalet individer i en rutas grannrutor. Funktionen ska returnera
    antalet grannar och ta argumenten världen och koordinaterna till rutan i fråga.
    Varje ruta har upp till åtta grannrutor - längs kanterna är de färre. Rutan snett ovan till vänster om en 
    godtycklig ruta har koordinaterna x-1, y-1, den till höger x+1, y, den rad under x, y-1 osv. Man kan använda 
    matrisen nedan för att hålla ordning på grannarna till godtycklig ruta.

    `# The relative neighbouring coordinates at any given point.`

    `neighbours_list = [( -1,  -1), ( -1, 0), ( -1, 1),`
    `                 (   0,  -1),           (  0, 1), `
    `                 (   1,  -1), (  1, 0), (  1, 1)]`
    Använd denna matris när du i tur och ordning anropar funktionen som du annoterade med `@life(4)` för att
    ta reda på alla grannrutornas invånare.

    Tagga sedan upp din nya funktion med `@life(5)` och testkör den.

    Det går att implementera `@life(6)` före denna om du vill.

  * Nu ska vi skapa en funktion till som använder de tre reglerna och utdata ifrån funktionen som räknar 
    ut antalet invånare i grannrutorna till en godtycklig ruta. Denna nya funktion ska för godtycklig
    ruta returnera om samma ruta i nästa värld ska vara bebodd eller inte.
    Den tredje regeln implementeras lättast genom att om varken regel 1 "Överlevnad" eller regel 2 "Födsel"
    returnerar `True` så inträffar regel 3 "Döden" automatiskt, dvs alla tre regler kan modelleras med
    `if-elif-else`.
   
    Funktionen ska ta förljande argument `world, x, y, num_neighbours`, där num_neighbours är antalet
    grannar till den godtyckliga rutan x, y. 

    Annotera funktionen med `@life(6)`. 
 
  * Nu är det dags att sy ihop det hela! Nu behövs en funktion som tar den nuvarande världen som inargument och
    returnerar nästa som utargument.

     - Börja med att först i funktionen anropa din funktion som du annoterade med `@life(1)` som returnerar
       en ny tom värld.

     - Sedan behövs det två stycken nästlade for-loopar som går över y resp x ifrån 0 till antalet rader resp kolumner.	
       Mao så här:
       	   for y in...:
	       	for x in ..:
		
     - Inuti den innersta for-loop:en, behövs tre anrop:
       	     För behövs ta reda på hur många grannar som rutan har genom att fråga funktionen annoterad med `@life(5)`.
	     Sedan anropa funktionen annoterad med `@life(6)` med antalet grannar som du fick ifrån `@life(5)`.
	     Och till sist, uppdatera i den nya världen rutan med vad du fick ut ifrån `@life(6)`.
	     Hjälp: världsobjektet har en `update` funktion, så du kan göra så här: <nya_världen>.update(x,y, värde)
     - Efter och utanför de två for-loop:arna, returnera den nya världen när den är klar.

    Annotera funktionen med `@life(7)` och testa den.

  * Och till sist, en evighets-loop som visar upp vad som händer.
    Börja med att ladda in initialvärlden genom att anropa `load_world()`. Efter den skapa en evighets-loop.
    I evighets-loop:en anropa `show_world` med den nuvarande världen som argument och sedan skapa nästa med
    ett anrop till din funktion annoterad med `@life(7)`.

  *  Kör spelet! Nu ska mönstret förändras för varje varv som evighets-loop:en körs, beroende på de tre reglerna.


### Extrauppgifter

   * Gör en inmatningsrutin som skickar med ett start mönster till `load_world()`.
     Formatet som `load_world()` använder ser ut så här:

     `pattern = ["  ***", " *  *", "*   *"]`

     tex:

     `load_world(pattern = ["  ***", " *  *", "*   *"])`

     Tips: Använd `input()` och en `while`-loop tills det kommer en tom rad och anta att inmatnigen är slut.

   * Gör en funktion som räknar ut antalet individer i världen och skicka med till `show_world()`.

     ex:
     `show_world(population = current_population)`

     där `current_population` är vad din funktion har räknat fram.

     Annotera funktionen med `@life(8)` och testa.

   * Skriv en funktion som kontrollerar om nuvarande och nästa värld är lika, och i så fall
     returnerar `True`, annars `False`
     
     tex:

     `if nothing_happens(world, next_world):`
     `	 break`

     Annotera funktionen med `@life(9)` och testa.
 
